var Metalsmith  = require('metalsmith'),
	markdown    = require('metalsmith-markdown'),
	layouts     = require('metalsmith-layouts'),
	browserSync = require('metalsmith-browser-sync');

Metalsmith(__dirname)
    .source('./src')
    .destination('./build')
    .use(markdown())
    .use(browserSync())
    .use(layouts({
        'engine': 'handlebars'
    }))
    .build(function (err) {
        if (err) throw err;
        else console.log('Build finished');
    });
